﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SingleShotGun : Gun
{
    [SerializeField] Camera cam;
    public GameObject personaje;
    PhotonView PV;
    PlayerController playercontrollerr;
   

    private void Awake()
    {
        PV = GetComponent<PhotonView>();
    }
    public override void Use()
    {
        
        Shoot();
    }
    void Shoot()
    {
        Ray ray = cam.ViewportPointToRay(new Vector3(0.5f, 0.5f));
        ray.origin = personaje.transform.position;
        
        if (Physics.Raycast(ray, out RaycastHit hit))
        {
            hit.collider.gameObject.GetComponent<IDamageable>()?.TakeDamage(((FireInfo)itemInfo).damage);
            PV.RPC("RPC_Shoot", RpcTarget.All, hit.point);
            
            
        }    
    }

    [PunRPC]
    void RPC_Shoot(Vector3 hitPosition)
    {
        Instantiate(parsystem, hitPosition, Quaternion.identity);
       
    }
}

