﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class ControlOrco : MonoBehaviour
{
    public Rigidbody rb;
    public float speed;
    public Animator ani;
    public Transform Eje;
    public bool inground;
    private RaycastHit hit;
    public float distance;
    public Vector3 v3;
   

    void FixedUpdate()
    {
        Move();
    }
    void Update()
    {
        if(Physics.Raycast(transform.position +v3, transform.up *-1, out hit, distance))
        {
            if(hit.collider.tag == "piso")
            {
                inground = true;
            }
            else
            {
                inground = false;
            }
        }
        if (Input.GetMouseButtonDown(0))
        {
            ani.SetBool("atacar", true);
        }
        else
        {
            ani.SetBool("atacar", false);
        }
        /*if(Input.GetKey(KeyCode.LeftShift))
        {
            
            speed = speed + 3;
            ani.SetBool("run", true);
        }
        else
        {
           

            ani.SetBool("walk", false);
        }*/
    }
    private void OnDrawGizmos()
    {
        Gizmos.DrawRay(transform.position + v3, Vector2.up * -1 * distance);
    }


    public void Move()
    {
        Vector3 rotaTargetZ = Eje.transform.forward;
        rotaTargetZ.y = 0;

        if (Input.GetKey(KeyCode.W))
        {
            transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.LookRotation(rotaTargetZ), 0.3f);
            var dir = transform.forward * speed * Time.fixedDeltaTime;
            dir.y = rb.velocity.y;
            rb.velocity = dir;
            ani.SetBool("walk", true);
        }
        else
        {
            if(inground)
            {
                rb.velocity = Vector3.zero;
            }
            ani.SetBool("walk", false);

        }
        if(Input.GetKey(KeyCode.S))
        {
            transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.LookRotation(rotaTargetZ* -1), 0.3f);
            var dir = transform.forward * speed * Time.fixedDeltaTime;
            dir.y = rb.velocity.y;
            rb.velocity = dir;
            ani.SetBool("walk", true);
        }
        Vector3 rotaTargetX = Eje.transform.right;
        rotaTargetX.y = 0;

        if (Input.GetKey(KeyCode.D))
        {
            transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.LookRotation(rotaTargetX), 0.3f);
            var dir = transform.forward * speed * Time.fixedDeltaTime;
            dir.y = rb.velocity.y;
            rb.velocity = dir;
            ani.SetBool("walk", true);
        }
        if (Input.GetKey(KeyCode.A))
        {
            transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.LookRotation(rotaTargetX * -1), 0.3f);
            var dir = transform.forward * speed * Time.fixedDeltaTime;
            dir.y = rb.velocity.y;
            rb.velocity = dir;
            ani.SetBool("walk", true);
        }
    }
}