﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamaraController : MonoBehaviour
{
    public GameObject target;
    public Vector3 vv3;
    public float speed;
    public float maxLook;
    public float minLook;
    public Quaternion camRotation;

    private void Start()
    {
        camRotation = transform.localRotation;
        
    }
    private void Awake()
    {
        Cam();
    }
    void Update()
    {
        Cam();
    }

    public void Cam()
    {
        
            transform.position = target.transform.position + vv3;
        

        camRotation.y += Input.GetAxis("Mouse X") * speed;
        camRotation.x += Input.GetAxis("Mouse Y") * speed* -1;

        camRotation.x = Mathf.Clamp(camRotation.x, minLook, maxLook);

        transform.localRotation = Quaternion.Euler(camRotation.x, camRotation.y, camRotation.z);
    }


}
