﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class CasasController : MonoBehaviour
{
    public int hp;
    public GameObject Casa;
    PhotonView view;

    void Start()
    {
        hp = 100;
        view = GetComponent<PhotonView>();
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Piña")
        {
            hp -= 15;
        }
        Debug.Log("Daño Casa");
    }

    void Update()
    {
        if (hp <= 0)
        {

            view.RPC("Destruir", RpcTarget.All);
        }
    }
    [PunRPC]
    void Destruir()
    {
        Destroy(Casa);
    }
}
