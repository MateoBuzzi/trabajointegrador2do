﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Arma/Nueva Arma")]
public class FireInfo : ItemInfo
{
    public float damage;
}
