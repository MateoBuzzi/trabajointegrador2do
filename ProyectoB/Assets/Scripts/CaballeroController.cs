﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;


public class CaballeroController : MonoBehaviour
{
    public int rutina;
    public float cronometro;
    public Animator anii;
    public Quaternion angulo;
    public float grado;
    GameObject target;
    public GameObject pj;
    public bool atacando;
    public float contador;
    public GameObject daño;
    int vida;
    public GameObject parsys;
    PhotonView biu;
    void Start()
    {
        anii = GetComponent<Animator>();
        target = GameObject.FindWithTag("Player");
        biu = this.gameObject.GetComponent<PhotonView>();
        atacando = false;
        vida = 100;
    }


    void Update()
    {
        biu.RPC("Comportamiento_Caballero", RpcTarget.All);


        if (vida <= 0)
        {
            Destroy(pj);
        }
    }
    [PunRPC]
    public void Comportamiento_Caballero()
    {

        if (Vector3.Distance(transform.position, target.transform.position) > 15)
        {


            cronometro += 1 * Time.deltaTime;
            if (cronometro >= 4)
            {
                rutina = Random.Range(0, 2);
                cronometro = 0;
            }
            switch (rutina)
            {
                case 0:
                    anii.SetBool("walk", false);
                    break;
                case 1:
                    grado = Random.Range(0, 360);
                    angulo = Quaternion.Euler(0, grado, 0);
                    rutina++;
                    break;
                case 2:
                    transform.rotation = Quaternion.RotateTowards(transform.rotation, angulo, 0.5f);
                    transform.Translate(Vector3.forward * 1 * Time.deltaTime);
                    anii.SetBool("walk", true);
                    break;
            }
        }
        else
        {
            if (Vector3.Distance(transform.position, target.transform.position) > 1.5f && !atacando)
            {
                var lookPos = target.transform.position - transform.position;
                lookPos.y = 0;
                var rotation = Quaternion.LookRotation(lookPos);
                transform.rotation = Quaternion.RotateTowards(transform.rotation, rotation, 2);

                anii.SetBool("walk", true);
                transform.Translate(Vector3.forward * 4 * Time.deltaTime);
                anii.SetBool("attack", false);

            }
            else
            {
                contador += 1 * Time.deltaTime;
                anii.SetBool("walk", false);
                anii.SetBool("attack", true);
                atacando = true;
                if (contador >= 10)
                {
                    atacando = false;
                }
            }

            if (contador >= 2)
            {
                anii.SetBool("attack", false);
                atacando = false;
                contador = 0;
                anii.SetBool("walk", true);
            }

            if (atacando == true)
            {
                daño.SetActive(true);
            }
            else
            {
                daño.SetActive(false);
            }

        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Pina"))
        {
            biu.RPC("Recibirdaño", RpcTarget.All);
            Debug.Log(vida);
        }
    }

    
    [PunRPC]
     void Recibirdaño()
    {
        vida -= 34;
    }
   

}
