﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using Hashtable = ExitGames.Client.Photon.Hashtable;

public class PlayerController : MonoBehaviourPunCallbacks, IDamageable
{
    [SerializeField] float mouseSensivility, sprintSpeed, walkSpeed, jumpForce, smoothTime;
    Rigidbody rb;
    [SerializeField] Camera cam;
    [SerializeField] GameObject cameraHolder;
    [SerializeField] Item[] items;
    bool grounded;
    Vector3 smoothMoveVelocity;
    Vector3 moveAmount;
    float verticalLookRotation;
    PhotonView PV;
    public Animator ani;
    public GameObject puño;
    const float maxHealth = 100f;
    float currentHealth;
    PlayerManager playermanager;
    int itemIndex;
    public GameObject proyectil;
    int previousItemIndex = -1;
    public GameObject origen;
    FireInfo fireinfo;
    public GameObject spawn;
    public GameObject pj;

    private void Awake()
    {
        rb = GetComponent<Rigidbody>();
        PV = GetComponent<PhotonView>();

        playermanager = PhotonView.Find((int)PV.InstantiationData[0]).GetComponent<PlayerManager>();
    }
     void Start()
    {
        currentHealth = maxHealth;
        
        if (PV.IsMine)
        {
            EquipItem(0);
            
                }
        else
        {
            Destroy(GetComponentInChildren<Camera>().gameObject);
            Destroy(rb);
        }
    }
    void EquipItem(int _index)
    {
        itemIndex = _index;

        items[itemIndex].itemGameObject.SetActive(true);

        if(previousItemIndex != -1)
        {
            items[previousItemIndex].itemGameObject.SetActive(false);
        }
        previousItemIndex = itemIndex;

        if(PV.IsMine)
        {
            Hashtable hash = new Hashtable();
            hash.Add("itemIndex", itemIndex);
            PhotonNetwork.LocalPlayer.SetCustomProperties(hash);
        }
    }
    public override void OnPlayerPropertiesUpdate(Player targetPlayer, Hashtable changedProps)
    {
        if(!PV.IsMine && targetPlayer == PV.Owner)
        {
            EquipItem((int)changedProps["itemindex"]);
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.CompareTag("Dano"))
            {
            currentHealth -= 10;
            Debug.Log(currentHealth);
        }
    }


    void TakeDamage()
    {
        PV.RPC("RPC_TakeDamage", RpcTarget.All, fireinfo.damage);
    }


    private void Update()
    {
        if (!PV.IsMine)
            return;
        Look();
        Move();
        Jump();
        Golpear();
        if (currentHealth <= 0)
        {
            Die();
        }


    }


    void Golpear() {

        if (Input.GetMouseButtonDown(0))
        {
            ani.SetBool("atacar", true);
            puño.SetActive(true);



        }
        else
        {
            puño.SetActive(false);
        }
            if (Input.GetMouseButtonDown(1))
            {
                ani.SetBool("atacar", true);
                items[itemIndex].Use();


                
            }
          
        
   

     

    }
    
    void Look()
    {
        transform.Rotate(Vector3.up * Input.GetAxisRaw("Mouse X") * mouseSensivility);

        verticalLookRotation += Input.GetAxisRaw("Mouse Y") * mouseSensivility;
        verticalLookRotation = Mathf.Clamp(verticalLookRotation, -50f, 45f);
        cameraHolder.transform.localEulerAngles = Vector3.left * verticalLookRotation;
    }
    void Move()
    {
        
        Vector3 moveDir = new Vector3 (Input.GetAxisRaw("Horizontal"), 0, Input.GetAxisRaw("Vertical")).normalized;
        moveAmount = Vector3.SmoothDamp(moveAmount, moveDir * (Input.GetKey(KeyCode.LeftShift) ? sprintSpeed : walkSpeed), ref smoothMoveVelocity, smoothTime);


        if (Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.D))
        {
            ani.SetBool("walk", true);
            ani.SetBool("atacar", false);
        }
        else
        {
            ani.SetBool("walk", false);
        }




    }

    
    void Jump()
    {
        if (Input.GetKeyDown(KeyCode.Space) && grounded)
        {
            rb.AddForce(transform.up * jumpForce);
            ani.SetBool("jump", true);
            ani.SetBool("atacar", false);
        }
        else
        {
            ani.SetBool("jump", false);
        }
        
        if (grounded == true)
        {
            ani.SetBool("jump", false);
        }
    }
    public void SetGroundedState(bool _grounded)
    {
        grounded = _grounded;
    }
    private void FixedUpdate()
    {
        rb.MovePosition(rb.position + transform.TransformDirection(moveAmount) * Time.deltaTime);

        if (!PV.IsMine)
            return;
    }
    public void TakeDamage(float damage)
    {
        PV.RPC("RPC_TakeDamage", RpcTarget.All, damage);
    }
[PunRPC]
void RPC_TakeDamage(float damage)
    {
        if (!PV.IsMine)
            return;
        currentHealth -= 34;

       
    }


    void Die()
    {
        pj.transform.position = spawn.transform.position;
        currentHealth = 100f;
        Debug.Log("Memori");
    }
}

